OBJS	= /*.o
SOURCE	= src/*.c parser/*.c
HEADER	= 
OUT	= a.out
CC	 = gcc
FLAGS	 = -g -c -Wall
LFLAGS	 = 

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)

*.o: src/*.c
	$(CC) $(FLAGS) src/*.c 



clean:
	rm -f $(OBJS) $(OUT)