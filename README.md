# Ardent Programming Language

# What is Ardent?

Ardent is a high level programming language designed to compile into WebAssembly.


## Installation


In-Progress

## Usage

```

func sum(arg1 : int, arg2: int) {
    return arg1 + arg2;
}

int myVar = 1;

println(sum(myVar, 1));

```

## Roadmap

A full roadmap will be posted in the future, for now I will lay a basic path that this project is expected to take.

I hope to have these roadmap features done by these dates..

**PARSER** - 12/01/2023


## Contributing

only from hiddendevs code-help


## Authors and acknowledgment

Maintainer - RatGrandmother

## License

This falls under the [GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html)

## Project status

Development will be slow, since I am at school most of my day. Except an update about every 3 days.
